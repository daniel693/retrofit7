package com.android.retrofit7;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetData {/*

//Specify the request type and pass the relative URL//

    @GET("/users")

//Wrap the response in a Call object with the type of the expected result//

    Call<List<RetroUsers>> getAllUsers();
*/

    //@GET("/api/location/search/?query={id}")
    @GET("/api/location/search/")
    public Call<List<City>> getCity(@Query("query") String id);

}