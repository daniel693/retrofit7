package com.android.retrofit7;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
//import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.List;

//Extend the RecyclerView.Adapter class//

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.CustomViewHolder> {

    private List<City> dataList;

    public MyAdapter(List<City> dataList){

        this.dataList = dataList;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

//Get a reference to the Views in our layout//

        public final View myView;

        TextView textCity;
        TextView textWoeId;

        CustomViewHolder(View itemView) {
            super(itemView);
            myView = itemView;

            textCity = myView.findViewById(R.id.user);
            textWoeId = myView.findViewById(R.id.woe);

        }
    }
    @Override

//Construct a RecyclerView.ViewHolder//

    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_layout, parent, false);
        return new CustomViewHolder(view);
    }



//Construct a RecyclerView.ViewHolder//



    @Override

//Set the data//

    public void onBindViewHolder(CustomViewHolder holder, int position) {
        holder.textCity.setText(dataList.get(position).getTitle());
        holder.textWoeId.setText(Integer.toString(dataList.get(position).getWoeid()));

    }

//Calculate the item count for the RecylerView//

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}