package com.android.retrofit7;

public class City {
    int distance;
    String title;
    String location_type;
    int woeid;
    String latt_long;

    //getMethods

    public int getDistance(){
        return this.distance;
    }
    public String getTitle(){
        return this.title;
    }
    public String getLocation_type(){
        return location_type;
    }
    public int getWoeid(){
        return this.woeid;
    }
    public String getLatt_long(){
        return this.latt_long;
    }

    //setMethods
    public void setDistance(int d){
        this.distance=d;
    }
    public void setTitle(String t){
        this.title=t;
    }
    public void setLocation_type(String loc){
        this.location_type=loc;
    }
    public void setWoeid(int w){
        this.woeid=w;
    }
    public void setLatt_long(String latt){
        this.latt_long=latt;
    }

    public String toString() {
        return "Title: '" + this.title + "', Location_Type '" + this.location_type + "', Woeid: '" + this.woeid + "', Latt_long: " + this.latt_long;
    }
}
